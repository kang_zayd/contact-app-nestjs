import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Contact } from './contact.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Contact)
    private readonly contactRepo: Repository<Contact>,
  ) {}

  async getAllContact(): Promise<Contact[]> {
    return await this.contactRepo.find({ select: ['id', 'name'] });
  }

  async getDetailContact(id: number): Promise<Contact> {
    try {
      return await this.contactRepo.findOneOrFail(id);
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async createContact(contact: Contact): Promise<boolean> {
    try {
      await this.contactRepo.save(contact);
      return true;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async editContact(idToUpdate: number, contact: Contact): Promise<boolean> {
    try {
      await this.contactRepo.update(idToUpdate, contact);
      return true;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async deleteContact(idToDelete: number): Promise<boolean> {
    try {
      await this.contactRepo.delete(idToDelete);
      return true;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }
}
