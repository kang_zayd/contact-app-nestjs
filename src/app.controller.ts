import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Contact } from './contact.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getListContact(): Promise<Contact[]> {
    return await this.appService.getAllContact();
  }

  @Get(':id')
  async getDetailContact(@Param('id') id: number): Promise<Contact> {
    return await this.appService.getDetailContact(id);
  }

  @Post()
  async createNewContact(@Body() contact: Contact): Promise<boolean> {
    return await this.appService.createContact(contact);
  }

  @Put(':id')
  async editContact(
    @Param('id') id: number,
    @Body() contact: Contact,
  ): Promise<boolean> {
    return await this.appService.editContact(id, contact);
  }

  @Delete(':id')
  async deleteContact(@Param('id') id: number): Promise<boolean> {
    return await this.appService.deleteContact(id);
  }
}
