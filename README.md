# NestJS Contact Application
by Zanuarestu Ramadhani <zanua.restu@live.com>

get the presentation [here](https://s.id/8u2Mu)

## Setup
- Copy `ormconfig.default.json` to `ormconfig.json`
``` bash
$ cp ormconfig.default.json ormconfig.json
```
- adjust the content of `ormconfig.json` according to your MySQL or MariaDB configuration

## Run Development Mode
``` bash
$ npm run start:dev
# Or using yarn 
$ yarn run start:dev
```

## Run Production Mode
``` bash
$ npm run start:prod
# Or using yarn 
$ yarn run start:prod
```